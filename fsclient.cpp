#include "fsclient.h"
#include <QtDebug>

APODFSCrawler::APODFSCrawler(QDir path, QObject *parent) : QObject(parent), path(path) {
}

bool APODFSCrawler::exists(const ImageMetaData& metadata) {
    QString prefix = filePrefix(metadata);
    QFile f(path.absoluteFilePath(prefix + ".dump"));
    return f.exists();
}

QString APODFSCrawler::filePrefix(const ImageMetaData& metadata) {
    return metadata.date.toString("ddMMyy");
}

void APODFSCrawler::run() {
    qDebug() << "FSCrawler started";
    QStringList filters("*.dump");
    ImageMetaData metadata;
    auto entries = path.entryInfoList(filters);
    for(auto& finfo : entries) {
        QFile image(path.absoluteFilePath(finfo.baseName() + ".jpg"));
        if(!image.exists()) {
            continue;
        }
        QFile dump(finfo.absoluteFilePath());
        dump.open(QIODevice::ReadOnly);
        QDataStream reader(&dump);
        reader >> metadata;
        dump.close();
        emit imageFound(metadata);
    }
    emit finished();
}

void APODFSCrawler::save(const ImageMetaData& metadata, const QImage image) {
    QString prefix = filePrefix(metadata);

    QFile meta(path.absoluteFilePath(prefix + ".dump.temp"));
    meta.open(QIODevice::WriteOnly);
    QDataStream writer(&meta);
    writer << metadata;
    meta.close();

    image.save(path.absoluteFilePath(prefix + ".jpg"));

    meta.rename(path.absoluteFilePath(prefix + ".dump"));
}
