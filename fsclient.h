#include <QObject>
#include <QDir>
#include <QImage>
#include "metadata.h"

class APODFSCrawler : public QObject {
    Q_OBJECT
public:
    APODFSCrawler(QDir path, QObject *parent=0);
    bool exists(const ImageMetaData&);
    void save(const ImageMetaData&, const QImage);

private:
    QDir path;
    QString filePrefix(const ImageMetaData&);

signals:
    void imageFound(ImageMetaData);
    void finished();

public slots:
    void run();
};
