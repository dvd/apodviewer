#include <QtDebug>
#include "models.h"

using namespace APOD;

QHash<int, QByteArray> ImagesModel::_initRolesNames() {
    QHash<int, QByteArray> roles;
    roles[Qt::DisplayRole] = "display";
    roles[Qt::UserRole + 0] = "date";
    roles[Qt::UserRole + 1] = "explanation";
    roles[Qt::UserRole + 2] = "preview";
    roles[Qt::UserRole + 3] = "image";
    return roles;
}

const QHash<int, QByteArray> ImagesModel::roles = ImagesModel::_initRolesNames();

ImagesModel::ImagesModel(QObject *parent) : QAbstractListModel(parent) {}

QHash<int, QByteArray> ImagesModel::roleNames() const {
    return roles;
}

int ImagesModel::rowCount(const QModelIndex &parent) const {
    return !parent.isValid() ? index.size() : 0;
}

QVariant ImagesModel::data(const QModelIndex &index, int role) const {
    if(role == Qt::DisplayRole) {
        auto key = this->index.keys()[index.row()];
        return this->index[key]->title;
    }
    else if(role == Qt::UserRole + 0) {
        auto key = this->index.keys()[index.row()];
        auto metadata = this->index[key];
        return metadata->date;
    }
    else if(role == Qt::UserRole + 1) {
        auto key = this->index.keys()[index.row()];
        auto metadata = this->index[key];
        return metadata->explanation;
    }
    else if(role == Qt::UserRole + 2) {
        auto key = this->index.keys()[index.row()];
        auto metadata = this->index[key];
        return "../cache/" + metadata->date.toString("ddMMyy") + ".jpg";
    }
    else if(role == Qt::UserRole + 3) {
        auto key = this->index.keys()[index.row()];
        auto metadata = this->index[key];
        return metadata->imageUrl;
    }
    else
        return QVariant();
}

void ImagesModel::addImage(const ImageMetaData *metadata) {
    auto key = metadata->date.toString("yyyyMMdd").toInt();
    int ix = 0;
    for(auto it : index.keys()) {
        if(it >= key)
            break;
        ix++;
    }
    emit beginInsertRows(QModelIndex(), ix, ix);
    index[key] = metadata;
    emit endInsertRows();
}

QHash<int, QByteArray> IndexerModel::_initRolesNames() {
    QHash<int, QByteArray> roles;
    roles[Qt::DisplayRole] = "display";
    roles[Qt::UserRole + 0] = "submodel";
    return roles;
}

const QHash<int, QByteArray> IndexerModel::roles = IndexerModel::_initRolesNames();

IndexerModel::IndexerModel(const APODIndexer& indexer, QObject *parent) : QAbstractListModel(parent) {
    QObject::connect(
        &indexer, SIGNAL(imageReady(const ImageMetaData*)),
        this, SLOT(onImageReady(const ImageMetaData*)));
}

QHash<int, QByteArray> IndexerModel::roleNames() const {
    return roles;
}

int IndexerModel::rowCount(const QModelIndex &parent) const {
    return !parent.isValid() ? index.size() : 0;
}

QVariant IndexerModel::data(const QModelIndex &index, int role) const {
    if(role == Qt::DisplayRole) {
        auto entry = this->index.keys()[index.row()];
        return entry ? QString::number(entry) : "today";
    }
    else
        return QVariant();
}

void IndexerModel::onImageReady(const ImageMetaData *metadata) {
    QDate today = QDate::currentDate();
    auto year = metadata->date == today ? 0 : metadata->date.year();
    if(index.find(year) == index.end()) {
        qDebug() << "INSERT";
        int ix = 0;
        for(auto it : index.keys()) {
            if(it >= year)
                break;
            ix++;
        }
        emit beginInsertRows(QModelIndex(), ix, ix);
        index[year] = new ImagesModel(this);
        emit endInsertRows();
    }
    index[year]->addImage(metadata);
}

QObject* IndexerModel::images(const QString& year) {
    int key = year == "today" ? 0 : year.toInt();
    return index[key];
}
