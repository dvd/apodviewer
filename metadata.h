#ifndef METADATA_H
#define METADATA_H

#include <QObject>
#include <QDate>
#include <QUrl>
#include <QDataStream>
#include <QMetaType>

struct ImageMetaData {
    QUrl    pageUrl;
    QString title;
    QDate   date;
    QUrl    imageUrl;
    QString explanation;
};
Q_DECLARE_METATYPE(ImageMetaData);

QDataStream& operator<<(QDataStream &, const ImageMetaData&);
QDataStream& operator>>(QDataStream &, ImageMetaData&);

#endif
