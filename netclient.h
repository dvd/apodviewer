#include <QObject>
#include <QtConcurrent/QFutureWatcher>
#include <QImage>
#include <QtNetwork>
#include "metadata.h"

class APODMainIndexDownloader : public QObject {
    Q_OBJECT
public:
    APODMainIndexDownloader(QNetworkAccessManager *manager, QObject *parent=0);

private:
    QNetworkReply *reply;
    void parseIndexRow(QString, QString, ImageMetaData*);

signals:
    void indexDownloaded(QList<ImageMetaData*>);

private slots:
    void requestFinished();
};

class APODImageMetaDataDownloader : public QObject {
    Q_OBJECT
public:
    APODImageMetaDataDownloader(QNetworkAccessManager*, ImageMetaData*);

private:
    ImageMetaData *image;
    QNetworkReply *reply;
    QFutureWatcher<QPair<QString, QString>> *parseWatcher;
    static QPair<QString, QString> parseHtml(QString);

signals:
    void finished(ImageMetaData*);

private slots:
    void requestFinished();
    void parseFinished();
};

class APODImageDownloader : public QObject {
    Q_OBJECT
public:
    APODImageDownloader(QNetworkAccessManager*, ImageMetaData*);
    static QSize thumbSize;

private:
    ImageMetaData *image;
    QNetworkReply *reply;
    QFutureWatcher<QImage> *resizeWatcher;
    static QImage resizeImage(QByteArray);

signals:
    void finished(ImageMetaData*, QImage);

private slots:
    void requestFinished();
    void resizeFinished();
};

