#include "index.h"
#include "netclient.h"
#include <QtWidgets/QApplication>

class AsyncFSCrawler : public QThread {
public:
    AsyncFSCrawler(APODFSCrawler *fs, QObject *parent=0);
    void run();
private:
    APODFSCrawler *fs;
};

AsyncFSCrawler::AsyncFSCrawler(APODFSCrawler *fs, QObject *parent) : QThread(parent), fs(fs) {
    fs->moveToThread(this);
}
void AsyncFSCrawler::run() {
    fs->run();
    fs->moveToThread(QApplication::instance()->thread());
}

APODIndexer::APODIndexer(QDir cache, QObject *parent) : QObject(parent), cache(cache), state(NOT_READY) {
    manager = new QNetworkAccessManager(this);
}

void APODIndexer::update() {
    auto t = new AsyncFSCrawler(&cache);
    QObject::connect(
        &cache, SIGNAL(imageFound(ImageMetaData)),
        this, SLOT(imageFound(ImageMetaData)));
    QObject::connect(
        &cache, SIGNAL(finished()),
        this, SLOT(fsCrawlerFinished()));
    QObject::connect(
        t, SIGNAL(finished()),
        t, SLOT(deleteLater()) );
    t->start();

    auto index = new APODMainIndexDownloader(manager);
    QObject::connect(
        index, SIGNAL(indexDownloaded(QList<ImageMetaData*>)),
        this, SLOT(indexDownloaded(QList<ImageMetaData*>)));
}

void APODIndexer::fsCrawlerFinished() {
    if(state == NOT_READY) {
        state = PARTIAL;
    }
    else {
        mergeIndexes();
    }
}

void APODIndexer::mergeIndexes() {
    qDebug() << "time to merge!" << mainIndex.size() << remoteIndex.size();
    state = COMPLETE;
    for(auto v : remoteIndex) {
        if(mainIndex.contains(v->pageUrl))
            continue;
        if(v->date.year() == 2012 && v->date.month() == 9) {
            auto d = new APODImageMetaDataDownloader(manager, v);
            QObject::connect(
                d, SIGNAL(finished(ImageMetaData*)),
                this, SLOT(imageMetaDataReady(ImageMetaData*)));
        }
    }
    remoteIndex.clear();
}

void APODIndexer::imageFound(ImageMetaData metadata) {
    qDebug() << "image found" << metadata.date.toString() << metadata.title;
    mainIndex[metadata.pageUrl] = metadata;
    emit imageReady(&mainIndex[metadata.pageUrl]);
}

void APODIndexer::indexDownloaded(QList<ImageMetaData*> index) {
    qDebug() << "remote index downloaded" << index.size();
    remoteIndex = index;

    if(state == NOT_READY) {
        state = PARTIAL;
        // in order to not waste bandwith we wait for the fs crawler to
        // complete; the only exception is for the first image, there is a good
        // chance that we need it just now.
        if(!cache.exists(*index[0])) {
            auto d = new APODImageMetaDataDownloader(manager, index[0]);
            QObject::connect(
                d, SIGNAL(finished(ImageMetaData*)),
                this, SLOT(imageMetaDataReady(ImageMetaData*)));
        }
    }
    else {
        mergeIndexes();
    }
}

void APODIndexer::imageMetaDataReady(ImageMetaData *img) {
    qDebug() << "image meta data ready" << img->date << img->title;
    auto d = new APODImageDownloader(manager, img);
    QObject::connect(
        d, SIGNAL(finished(ImageMetaData*, QImage)),
        this, SLOT(imageDownloaded(ImageMetaData*, QImage)));
}

void APODIndexer::imageDownloaded(ImageMetaData *i, QImage img) {
    qDebug() << "image downloaded... writing in cache";
    cache.save(*i, img);
    imageFound(*i);
    delete i;
}

