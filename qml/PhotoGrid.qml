// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 2.0
import "PhotoGrid.js" as Core

Rectangle {
    id: container
    border {
        width: 1
    }

    signal imageSelected(variant data);

    Component {
        id: gridComponent
        Rectangle {
            id: grid

            property alias model: grid_view1.model

            color: "red"
            z: 1
            clip: true
            x: 0
            y: 0
            width: parent.width
            height: parent.height
            transitions: [
                Transition {
                    from: ""
                    to: "HIDDEN"
                    NumberAnimation {
                        properties: "x,opacity"
                        easing.type: Easing.InOutQuint
                        duration: 800
                    }
                },
                Transition {
                    from: "HIDDEN"
                    to: ""
                    NumberAnimation {
                        properties: "x,opacity"
                        easing.type: Easing.Linear
                        duration: 200
                    }
                }
            ]
            states: [
                State {
                    name: "HIDDEN"
                    PropertyChanges {
                        target: grid
                        x: parent.width
                        opacity: 0
                        z: 0
                    }
                }
            ]

            GridView {
                id: grid_view1
                cellWidth: 320
                cellHeight: 245
                anchors {
                   fill: parent
                }
                delegate: Rectangle {
                    width: 320
                    height: 245
                    border {
                        width: 1
                        color: "white"
                    }
                    color: "yellow"
                    anchors {
                        margins: 10
                    }

                    Text {
                        anchors {
                            bottom: preview.bottom
                            left: preview.left
                        }
                        text: model.display
                        z: 1
                        color: "white"
                    }
                    Image {
                        id: preview
                        source: model.preview
                        width: 300
                        height: 225
                        anchors {
                            horizontalCenter: parent.horizontalCenter
                            verticalCenter: parent.verticalCenter
                        }
                        fillMode: Image.Stretch
                        MouseArea {
                            anchors {
                                fill: parent
                            }
                            onClicked: container.imageSelected({
                                title: model.display,
                                date: model.date,
                                explanation: model.explanation,
                                image: model.image})
                        }
                    }
                }
            }
        }
    }

    function showImages(model) {
        Core.showModel(model);
    }
}


