var grids = {};
var current = null;

function showModel(model) {
    if(!(model in grids)) {
        var new_grid = gridComponent.createObject(container, {state: "HIDDEN"});
        grids[model] = new_grid;
    }
    else {
        new_grid = grids[model];
    }

    if(current === new_grid) {
        return
    }

    if(current) {
        current.state = "HIDDEN";
    }

    new_grid.model = model;
    new_grid.state = "";
    current = new_grid;
}
