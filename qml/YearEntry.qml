import QtQuick 2.0

Rectangle {
    id: button

    radius: 5
    border {
        width: 1
    }

    width: 100
    height: 40

    FontLoader {
        id: chalkFont
        source: "./Drawing Guides.ttf"
    }
    Text {
        color: "#ffee66"
        text: display
        verticalAlignment: Text.AlignTop
        horizontalAlignment: Text.AlignHCenter
        font.bold: true
        font.pointSize: 18
        font.family: chalkFont.name
        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }
    }

    MouseArea {
        id: mouse_area1
        anchors {
            fill: parent
        }
        onClicked: parent.clicked()
    }

    signal clicked();
}
