#include "metadata.h"

QDataStream& operator<<(QDataStream &stream, const ImageMetaData &image) {
    stream << image.pageUrl << image.title << image.date << image.imageUrl << image.explanation;
    return stream;
}

QDataStream& operator>>(QDataStream &stream, ImageMetaData &image) {
    stream >> image.pageUrl >> image.title >> image.date >> image.imageUrl >> image.explanation;
    return stream;
}

