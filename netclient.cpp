#include "netclient.h"
#include <QtConcurrent/QtConcurrent>
#include <QtDebug>
#include <QPainter>
#include <QRegExp>
#include <htmlcxx/html/ParserDom.h>

APODMainIndexDownloader::APODMainIndexDownloader(QNetworkAccessManager *manager, QObject *parent) : QObject(parent) {
    qDebug() << "start remote index downloading";
    QUrl index(QUrl("http://apod.nasa.gov/apod/archivepix.html"));
    reply = manager->get(QNetworkRequest(index));
    QObject::connect(
        reply, SIGNAL(finished()),
        this, SLOT(requestFinished()));
}

void APODMainIndexDownloader::requestFinished() {
    qDebug() << "index downloaded";

    QList<ImageMetaData*> index;
    QString doc(reply->readAll());
    QRegExp rx("<a href=\"(ap\\d{6}.html)\">([^<]+)</a>");
    int pos = 0;
    while ((pos = rx.indexIn(doc, pos)) != -1) {
        QStringList groups = rx.capturedTexts();
        auto metadata = new ImageMetaData();
        parseIndexRow(groups[1], groups[2], metadata);
        index.append(metadata);
        pos += rx.matchedLength();
    }
    emit indexDownloaded(index);

    reply->deleteLater();
    deleteLater();
}

void APODMainIndexDownloader::parseIndexRow(QString pagename, QString title, ImageMetaData *metadata) {
    int year = pagename.mid(2, 2).toInt();
    int month = pagename.mid(4, 2).toInt();
    int day = pagename.mid(6, 2).toInt();
    year += year >= 95 ? 1900 : 2000;

    metadata->pageUrl = QUrl("http://apod.nasa.gov/apod/"  + pagename);
    metadata->title = title;
    metadata->date = QDate(year, month, day);
}

APODImageMetaDataDownloader::APODImageMetaDataDownloader(QNetworkAccessManager *manager, ImageMetaData *img) : image(img) {
    parseWatcher = new QFutureWatcher<QPair<QString, QString>>(this);
    QObject::connect(
        parseWatcher, SIGNAL(finished()),
        this, SLOT(parseFinished()));

    qDebug() << img->pageUrl;
    reply = manager->get(QNetworkRequest(img->pageUrl));
    QObject::connect(
        reply, SIGNAL(finished()),
        this, SLOT(requestFinished()));
}

void APODImageMetaDataDownloader::requestFinished() {
    qDebug() << "page downloaded";
    auto future = QtConcurrent::run(APODImageMetaDataDownloader::parseHtml, reply->readAll());
    parseWatcher->setFuture(future);
    reply->deleteLater();
}

void APODImageMetaDataDownloader::parseFinished() {
    qDebug() << "parse finished";
    auto result = parseWatcher->future().result();
    image->imageUrl = image->pageUrl.resolved(QUrl(result.first));
    image->explanation = result.second;

    emit finished(image);
    deleteLater();
}

QPair<QString, QString> APODImageMetaDataDownloader::parseHtml(QString doc) {
    using namespace htmlcxx;
    HTML::ParserDom parser;
    tree<HTML::Node> dom = parser.parseTree(doc.toStdString());

    QString imageSrc;
    QString explanation;

    auto node = dom.begin();
    auto end = dom.end();
    for(; node != end; ++node) {
        auto tag = QString::fromStdString(node->tagName()).toLower();
        if(tag == "img") {
            auto anchor = dom.parent(node);
            anchor->parseAttributes();
            auto src = anchor->attribute("href");
            if(src.first) {
                imageSrc = QString::fromStdString(src.second);
            }
        }
        else if(!node->isTag() && !node->isComment()) {
            auto text = QString::fromStdString(node->text()).trimmed();
            if(text.mid(0, 11).toLower() == "explanation") {
                QStringList expl;
                auto xit = dom.next_sibling(dom.parent(node));
                while(dom.is_valid(xit)) {
                    auto text = QString::fromStdString(xit->text());
                    if(text.toLower().trimmed().mid(0, 18) == "tomorrow's picture") {
                        break;
                    }
                    expl<< text;
                    xit = dom.next_sibling(xit);
                }
                auto purge = expl.end();
                auto purge_end = expl.begin();
                while(purge != purge_end) {
                    --purge;
                    if(purge->trimmed().length() == 0 || ((*purge)[0] == '<' && (*purge)[1] != '/')) {
                        purge = expl.erase(purge);
                    }
                    else {
                        break;
                    }
                }
                explanation = expl.join("");
            }
        }
    }

    return qMakePair(imageSrc, explanation);
}

QSize APODImageDownloader::thumbSize = QSize(800, 600);

APODImageDownloader::APODImageDownloader(QNetworkAccessManager *manager, ImageMetaData *img) : image(img) {
    resizeWatcher = new QFutureWatcher<QImage>(this);
    QObject::connect(
        resizeWatcher, SIGNAL(finished()),
        this, SLOT(resizeFinished()));

    reply = manager->get(QNetworkRequest(image->imageUrl));
    QObject::connect(
        reply, SIGNAL(finished()),
        this, SLOT(requestFinished()));
}

void APODImageDownloader::requestFinished() {
    auto future = QtConcurrent::run(APODImageDownloader::resizeImage, reply->readAll());
    resizeWatcher->setFuture(future);
    reply->deleteLater();
}

QImage APODImageDownloader::resizeImage(QByteArray buffer) {
    auto img = QImage::fromData(buffer);
    if(img.isNull()) {
        return img;
    }

    auto size = img.size();
    if(size.width() > thumbSize.width() || size.height() > thumbSize.height()) {
        img = img.scaled(thumbSize, Qt::KeepAspectRatio);
        size = img.size();
    }
    if(size != thumbSize) {
        auto enlarged = QImage(thumbSize, QImage::Format_RGB32);
        enlarged.fill(Qt::black);
        QPainter painter(&enlarged);
        painter.drawImage(
            (thumbSize.width() - size.width()) / 2,
            (thumbSize.height() - size.height()) / 2,
            img);
        painter.end();
        return enlarged;
    }
    else {
        return img;
    }
}

void APODImageDownloader::resizeFinished() {
    auto result = resizeWatcher->future().result();
    emit finished(image, result);
    deleteLater();
}
