#ifndef INDEX_H
#define INDEX_H

#include <QObject>
#include <QImage>
#include <QtNetwork>
#include "metadata.h"
#include "fsclient.h"

class APODIndexer : public QObject {
    Q_OBJECT
public:
    APODIndexer(QDir cache, QObject *parent=0);
    void update();
    enum State { NOT_READY, PARTIAL, COMPLETE };

private:
    APODFSCrawler cache;
    QNetworkAccessManager *manager;
    QHash<QUrl, ImageMetaData> mainIndex;
    QList<ImageMetaData*> remoteIndex;
    State state;
    void mergeIndexes();

public slots:
    void fsCrawlerFinished();
    void indexDownloaded(QList<ImageMetaData*>);
    void imageMetaDataReady(ImageMetaData*);
    void imageDownloaded(ImageMetaData*, QImage);
    void imageFound(ImageMetaData);

signals:
    void imageReady(const ImageMetaData*);
};
#endif
