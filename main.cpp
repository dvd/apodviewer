#include <QtWidgets/QApplication>
#include <QDir>
#include <QtDebug>
#include <QMetaType>
#include "index.h"
#include "models.h"

#include <QTimer>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QListView>

#include <QQuickView>
#include <QQmlContext>
#include <QQmlEngine>

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    qRegisterMetaType<ImageMetaData>();
    qDebug() << "ready to go";

    QDir cache("./cache");
    if(!cache.exists()) {
        QDir(".").mkdir("cache");
    }

    APODIndexer index(cache);
    APOD::IndexerModel mainModel(index);
    index.update();

    QQuickView view;
    view.rootContext()->setContextProperty("mainModel", &mainModel);
    view.setSource(QUrl::fromLocalFile("main.qml"));
    view.show();

    return app.exec();
}
