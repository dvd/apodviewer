#ifndef MODELS_H
#define MODELS_H

#include <QObject>
#include <QAbstractListModel>
#include "index.h"
#include "metadata.h"

namespace APOD {

class ImagesModel : public QAbstractListModel {
    Q_OBJECT
public:
    const static QHash<int, QByteArray> roles;

    ImagesModel(QObject *parent=0);
    virtual int rowCount(const QModelIndex &parent=QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const;
    virtual QHash<int, QByteArray> roleNames() const;

    void addImage(const ImageMetaData*);

private:
    static QHash<int, QByteArray> _initRolesNames();
    QMap<int, const ImageMetaData*> index;
};

class IndexerModel : public QAbstractListModel {
    Q_OBJECT
public:
    const static QHash<int, QByteArray> roles;

    IndexerModel(const APODIndexer& indexer, QObject *parent=0);
    virtual int rowCount(const QModelIndex &parent=QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const;
    virtual QHash<int, QByteArray> roleNames() const;

    Q_INVOKABLE QObject* images(const QString& year);
private slots:
    void onImageReady(const ImageMetaData*);

private:
    static QHash<int, QByteArray> _initRolesNames();
    QMap<int, ImagesModel*> index;
};

}
#endif // MODELS_H
