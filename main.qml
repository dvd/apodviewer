// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 2.0
import "qml"

Rectangle {
    id: root
    width: 1024
    height: 768
    color: "#64bad1"

    Text {
        id: header
        x: 206
        y: 19
        text: "APOD Viewer"
        style: Text.Normal
        font.pointSize: 36
        MouseArea {
            id: mouse_area1
            anchors {
                fill: parent
            }
            onClicked: zoom.show({
                title: "XXX",
                date: null,
                image: "http://apod.nasa.gov/apod/image/1209/airglow120824_ladanyi_1800px.jpg",
                explanation: "<a href=\"http://www.google.com/\">ggg</a>"});
        }
    }
    Rectangle {
        id: zoom
        z: 20
        color: "transparent"
        visible: false
        anchors {
            fill: parent
        }
        Rectangle {
            id: tissue
            color: "red"
            opacity: 0.3
            anchors {
                fill: parent
            }
        }
        Image {
            id: image
            fillMode: Image.PreserveAspectFit
            anchors {
                fill: parent
                margins: 60
            }
            Text {
                id: title
                color: "#ffffff"
                font {
                    pointSize: 20
                    family: "Impact"
                }
                anchors {
                    top: parent.top
                    right: parent.right
                    left: parent.left
                    margins: 10
                }
            }
            Text {
                id: explanation
                color: "#ffffff"
                font {
                    pointSize: 8
                    family: "Verdana"
                }
                textFormat: Text.RichText
                wrapMode: Text.Wrap
                anchors {
                    right: parent.right
                    bottom: parent.bottom
                    left: parent.left
                    margins: 10
                }
            }
        }

        function show(data) {
            image.source = data.image
            explanation.text = data.explanation
            title.text = data.title
            zoom.visible = true
        }
    }

    Column {
        spacing: 10
        Repeater {
            id:yearsColumn
            model: mainModel
            delegate: YearEntry {
                onClicked: wall.showImages(mainModel.images(model.display))
            }
        }
    }

    PhotoGrid {
        id: wall
        x: 206
        y: 88
        width: 795
        height: 658
        color: "green"
        onImageSelected: zoom.show(data)
    }
}
